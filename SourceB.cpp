#include "sqlite3.h"
#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>
  
using namespace std;

unordered_map<string, vector<string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	printf("%d",carPurchase(4,18,db,zErrMsg));
	printf("%d",carPurchase(5,19,db,zErrMsg));
	printf("%d",carPurchase(9,20,db,zErrMsg));
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3 * db, char * zErrMsg)
{
	stringstream ss;
	int buyerbalance;
	int carprice;
	int rc;
	rc = sqlite3_exec(db, "select available from cars where id = "+carid, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	if (stoi(results["available"][0]) == 1)
	{ 
		rc = sqlite3_exec(db, "select balance from accounts where Buyer_id = " + buyerid, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		buyerbalance = stoi(results["balance"][0]);
		rc = sqlite3_exec(db, "select price from cars where id = " + carid, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		carprice = stoi(results["price"][0]);
		if (buyerbalance >= carprice)
		{
			ss << "update accounts set balance = balance - " << carprice << " WHERE Buyer_id = " << buyerid;
			rc = sqlite3_exec(db, ss.str().c_str(), NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 0;
			}
			ss.clear();
			rc = sqlite3_exec(db, "update cars set available = 0 where Buyer_id = "+carid, NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 0;
			}
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
	
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	stringstream ss;
	int rc;
	rc = sqlite3_exec(db, "select balance from accounts where Buyer_id = " + from, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	if (stoi(results["balance"][1]) < amount)
	{
		return 0;
	}
	else
	{
		ss << "UPDATE accounts SET balance = balance - " << amount << " WHERE Buyer_id = " << from;
		rc = sqlite3_exec(db, ss.str().c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		ss.clear();
		ss << "UPDATE accounts SET balance = balance +" << amount << " WHERE Buyer_id = " << to;
		rc = sqlite3_exec(db, ss.str().c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 0;
		}
		ss.clear();
		return 1;
	}
}